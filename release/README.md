# Release
For now, just prints a summary of the tickets that were found.


## Requirements
1. [Pipenv](https://github.com/pypa/pipenv)
1. A [Jira Cloud API token](https://id.atlassian.com/manage/api-tokens) for an Atlassian account with `Read` access to at least one Jira instance.


## Setup
```shell
pipenv install
```


## Usage
```shell
pipenv run release.py 'fixVersion=[version name]'
```


## Output
```shell
- [XY-1234] (IssueType): Lorem ipsum dolor sit amet in Component1 and Component2
```


## Tip
Tired of entering your Jira credentials every time?  Make a settings file!
```sh
cp settings.example.yml settings.yml
```

Sample `settings.yml` file:
```yaml
---
site: example  # for 'example.atlassian.net'
user: jsmith@example.com
token: 1a2b3c4d5f6g7h8i9j0k1l2m
```

_(Note: `settings.yml` is protected by `.gitignore` to prevent accidents!)_


## License
[BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)


## Roadmap
Automate status changes and fixVersion Release.

#! /usr/bin/env python3
# Modules for Jira interactions
import requests      # for API requests
from requests.auth import HTTPBasicAuth  # for API requests
import json          # for API responses

# Modules for credentials/settings
import os.path       # check if settings file exists
from os import path  # (same thing)
import yaml          # settings file
import getpass       # interactive (sensitive) input (if no settings file)
import sys           # argument input for JQL query via CLI argument #1

# Handle settings/arguments
SETTINGS = yaml.full_load(open('settings.example.yml'))

if path.isfile('settings.yml'):  # if custom file exists
    SETTINGS.update(yaml.full_load(open('settings.yml')))  # override defaults

site = SETTINGS['site'] or input('Subdomain of Jira Cloud instance: ')
user = SETTINGS['user'] or input('Username for ' + site + '.atlassian.net: ')
token = SETTINGS['token'] or getpass.getpass('Personal API Token: ')

query = sys.argv[1] or input('Query (JQL): ')  # ex: 'key = XY-1234'

auth = HTTPBasicAuth(user, token)
base_url = 'https://' + site + '.atlassian.net/rest/api/2/'

def getIssuesBySearch(query, startAt=0):
    return requests.get(
        base_url + '/search',
        headers={'Accept': 'application/json'},
        auth=auth,
        params={
            'jql': query,
            'startAt': startAt,
            'fields': 'key, issuetype, summary, components, timespent'
        }
    )

startAt = 0
issues = []
while True:  # handle pagination
    issues_response = json.loads(getIssuesBySearch(query, startAt).text)
    issues += issues_response['issues']
    startAt += issues_response['maxResults']  # "pagination size"
    if issues_response['total'] <= startAt:
        break

for issue in issues:
    key = issue['key']
    summary = issue['fields']['summary']
    issuetype = issue['fields']['issuetype']['name']
    components = [component['name'] for component in issue['fields']['components']]
    timeSpentHours = (issue['fields']['timespent'] or 0)/60/60  # 'timespent' is in seconds

    # Anything below the threshold is not worth mentioning
    if timeSpentHours >= SETTINGS['changelog_threshold_hours']:
        print(f'- [{key}] ({issuetype}): {summary} in ' + ' and '.join(components))

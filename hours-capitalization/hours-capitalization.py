#! /usr/bin/env python3
# Modules for Jira interactions
import requests      # for API requests
from requests.auth import HTTPBasicAuth  # for API requests
import json          # for API responses

# Modules for credentials/settings
import os.path       # check if settings file exists
from os import path  # (same thing)
import yaml          # settings file
import getpass       # interactive (sensitive) input (if no settings file)

# Modules for processing
from datetime import datetime
from datetime import timezone

# Handle settings/arguments/user input
SETTINGS = yaml.full_load(open('settings.example.yml'))

if path.isfile('settings.yml'):  # if custom file exists
    SETTINGS.update(yaml.full_load(open('settings.yml')))  # override defaults

## Auth
site = SETTINGS['site'] or input('Subdomain of Jira Cloud instance: ')
user = SETTINGS['user'] or input('Username for ' + site + '.atlassian.net: ')
token = SETTINGS['token'] or getpass.getpass('Personal API Token: ')

#'i# Query (JQL) parameters
project = SETTINGS['project'] or input('Jira Project key: ')
start_date = SETTINGS['start'] or input('Start date (yyyy-mm-dd UTC): ')
end_date = SETTINGS['end'] or input('End date (yyyy-mm-dd UTC): ')
statuses = SETTINGS['statuses'] or input('Statuses (ex: ToDo,In Progress,Done): ').split(',')

### Convert date STRINGS to date OBJECTS
start_date = datetime.strptime(str(start_date), '%Y-%m-%d').replace(tzinfo=timezone.utc)
end_date = datetime.strptime(str(end_date), '%Y-%m-%d').replace(tzinfo=timezone.utc)


# Prepare authentication
auth = HTTPBasicAuth(user, token)
base_url = 'https://' + site + '.atlassian.net/rest/api/2/'


# Store results for the reports
hours_log = {}  # .{worklog.user.key}.{expense} = <worklog hours>
hours_ticket = {}  # .{user_field.key}.{expense} = <ticket hours>
changes_log = {expense:{} for expense in SETTINGS['expenses'].keys()}  # worklogtotal >= threshold
changes_ticket = {expense:{} for expense in SETTINGS['expenses'].keys()}  # timespent >= threshold


# Define helper functions
def getExpenseType(issuetype):
    for expense, issuetypes in SETTINGS['expenses'].items():
        if issuetype in issuetypes:
            return expense
    return None

def getIssueWorklogs(issueIdOrKey):
    return requests.get(
        base_url + '/issue/' + issueIdOrKey + '/worklog',
        headers={'Accept': 'application/json'},
        auth=auth
    )

def getIssuesBySearch(project, statuses, start_date, end_date, page_startAt=0):
    # Construct the JQL query
    query = 'project = ' + project
    query += ' AND status WAS IN (' + ','.join(statuses) + ')'
    query += ' DURING (' + str(start_date.strftime('%Y-%m-%d')) + ', ' + str(end_date.strftime('%Y-%m-%d')) + ')'

    # Determine which fields to request in the response
    fields = [
        'key',
        'issuetype',
        'summary',
        'components',
        'timespent'
    ]
    if SETTINGS['ticket_contributor_field']:  # ensure inclusion of this field
        fields.append(SETTINGS['ticket_contributor_field'])

    # Make the API call
    return requests.get(
        base_url + '/search',
        headers={'Accept': 'application/json'},
        auth=auth,
        params={
            'jql': query,
            'startAt': page_startAt,
            'fields': ','.join(fields)
        }
    )


# Collect search results
page_startAt = 0
issues = []
while True:  # handle pagination
    issues_response = json.loads(getIssuesBySearch(project, statuses, start_date, end_date, page_startAt).text)
    issues += issues_response['issues']
    page_startAt += issues_response['maxResults']  # "pagination size"
    if issues_response['total'] <= page_startAt:
        break


# Restructure search result data to suit our needs
for issue in issues:
    fields = issue['fields']  # convenient shorthand
    key = issue['key']
    summary = fields['summary']
    issuetype = fields['issuetype']['name']
    components = [component['name'] for component in fields['components']]
    timeSpentHours = (fields['timespent'] or 0)/60/60  # 'timespent' is measured in seconds


    # Hours Capitalization
    ## Old version - Ticket-level hours attribution
    if SETTINGS['ticket_contributor_field'] and timeSpentHours > 0:  # ignore 0 hours'
        contributor = fields[SETTINGS['ticket_contributor_field']]
        contributor = contributor['emailAddress'] if contributor else 'N/A'  # fallback if no data

        ### CapEx/OpEx data
        if contributor not in hours_ticket.keys():  # default all to '0' if not exists
            hours_ticket[contributor] = {expense:0 for expense in SETTINGS['expenses'].keys()}
        hours_ticket[contributor][getExpenseType(issuetype)] += timeSpentHours

        ### Changes data
        if timeSpentHours >= SETTINGS['changelog_threshold_hours']:
            changes_ticket[getExpenseType(issuetype)][key] = summary + ' in ' + ' and '.join(components)


    ## New version - Worklog-level hours attribution
    worklogs_response = getIssueWorklogs(issue['id'])
    worklogs = json.loads(worklogs_response.text)['worklogs']
    for worklog in worklogs: # 'worklog' = a single log entry
        entry_date = datetime.strptime(worklog['started'], '%Y-%m-%dT%H:%M:%S.%f%z')

        if entry_date >= start_date and entry_date <= end_date:
            timeSpentHours = worklog['timeSpentSeconds']/60/60
            contributor = worklog['author']['emailAddress']

            ### CapEx/OpEx data
            if contributor not in hours_log.keys():  # default all to '0' if not exists
                hours_log[contributor] = {expense:0 for expense in SETTINGS['expenses'].keys()}
            hours_log[contributor][getExpenseType(issuetype)] += timeSpentHours

            ### Changes data
            if timeSpentHours >= SETTINGS['changelog_threshold_hours']:
                changes_log[getExpenseType(issuetype)][key] = summary + ' in ' + ' and '.join(components)


# Reports!!!
## Changes
def generateChangelog(changes):
    for expense in changes.keys():
        print(expense)
        for key, summary in changes[expense].items():
            print('- ' + summary)  # list item
        print()  # blank line

## Expense Report
def generateExpenseReport(hours):
    # Massage the data into an easily-writable format for a CSV
    # NOTE: Some of this may look unnecessarily complex, but that's because...
    # ...it is fully dynamic, based on SETTINGS['expenses'].
    expenses = [expense for expense in SETTINGS['expenses'].keys()]
    rows = {}  # "CSV file rows"
    for name, time in hours.items():
        total = sum([hours for hours in time.values()])  # used to find % of time per expense
        rows[name] = {'Name': name}  # name is the first column
        for expense in expenses:  # NOTE: each expense gets a '%' column for "it vs. total time"
            rows[name][expense] = str(round(float(time[expense]), 1)).rstrip('0').rstrip('.')
            rows[name][expense + '%'] = str(round(float(time[expense]) / (total or 1) * 100, 1)).rstrip('0').rstrip('.')
        rows[name]['Total'] = str(round(total, 1)).rstrip('0').rstrip('.')  # tack on the total at the end

    # Header row
    columns = []
    for field in rows[list(rows.keys())[0]].keys():  # first row's keys = column names
        columns.append(field)
    print(','.join(columns))

    # Data rows
    for key, details in rows.items():
        row = []
        for column in columns:
            row.append(str(details[column]))
        print(','.join(row))


## Print reports
if SETTINGS['ticket_contributor_field']:
    print('TICKET-LEVEL REPORTS')

    print('CHANGES (>=' + str(SETTINGS['changelog_threshold_hours']) + 'hrs)')
    generateChangelog(changes_ticket)

    print()  # blank line
    print('HOURS')
    generateExpenseReport(hours_ticket)

    print()  # blank line
    print('==================================================')
    print()
    print('WORKLOG REPORTS')  # only introduce the next section if THIS ONE exists


print('CHANGES (>=' + str(SETTINGS['changelog_threshold_hours']) + 'hrs)')
generateChangelog(changes_log)

print()
print('HOURS')
generateExpenseReport(hours_log)

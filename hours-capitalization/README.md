# Hours Capitalization
Given a JQL query, generates two things for monthly finance reporting based on the Issues returned from that JQL query:

1. A summary changelog of any major items that were worked on.
1. A CSV of how much of each person's time was a capitalizable expense vs. operating expense.


## Requirements
1. [Pipenv](https://github.com/pypa/pipenv)
1. A [Jira Cloud API token](https://id.atlassian.com/manage/api-tokens) for an Atlassian account with `Read` access to at least one Jira instance.


## Setup
Just run `pipenv install`.


## Usage
1. Run `pipenv run ./hours-capitalization.py`.
1. Use the interactive prompts to provide any required information.

```
Subdomain of Jira Cloud instance: example
Username for example.atlassian.net: jsmith@example.com
Personal API Token: 🔑
Jira Project key: TEST
Start date (yyyy-mm-dd UTC): 2019-11-01
End date (yyyy-mm-dd UTC): 2019-11-14
Statuses (ex: ToDo,In Progress,Done): Doing,Testing
```


## Output
```
TICKET-LEVEL REPORTS
CHANGES (>=4hrs)
CapEx
- Lorem ipsum in Component1
- Dolor sit in Component1
- Amet in Component2

OpEx
- Consectetur in Component2 and Component3
- Adipiscing elit in Component1

HOURS
Name,CapEx,CapEx%,OpEx,OpEx%,Other,Other%,Total
user1,277.9,99.6,1,0.4,0,0,278.9
user2,70.3,72.3,26.9,27.7,0,0,97.2
user3,66.8,88,8.1,10.6,1,1.3,75.9
user4,61.9,49.4,63.4,50.6,0,0,125.3
user5,2,2.7,73,97.3,0,0,75

==================================================

WORKLOG REPORTS
CHANGES (>=4hrs)
CapEx
- Lorem ipsum in Component1
- Dolor sit in Component1

OpEx
- Consectetur in Component2 and Component3

HOURS
Name,CapEx,CapEx%,OpEx,OpEx%,Other,Other%,Total
user1,131.5,97.8,2.9,2.2,0,0,134.4
user2,57,68.6,26.1,31.4,0,0,83.1
user3,0.1,3.8,2.1,96.2,0,0,2.2
user4,11.5,16.4,58.5,83.6,0,0,70
user5,2,6.2,30,93.8,0,0,3
```


## Tip
You may create a `settings.yml` file to avoid answering the interactive prompts every time.

_(Note: `settings.yml` is already protected by `.gitignore` to prevent accidents!)_

Here's an example that provides the auth details and a custom field to report on, but still receives the "project" and "date range" prompts:
```yaml
---
site: example  # for 'example.atlassian.net'
user: jsmith@example.com
token: 1a2b3c4d5f6g7h8i9j0k1l2m
ticket_contributor_field: customfield_12345  # uses Jira CF[12345]
```

(See `settings.example.yml` for the full list of options.)


## License
[BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)


## Roadmap
- Embed Issues' worklogs in the original 'search' request.
	- Current behavior (inefficient):
		1. Fetch list of Issues
		1. Fetch worklog for EACH Issue

# Jira Scripts
Various scripts that help me to automate things via the Jira API.

Most scripts come with their own README, so be sure to check for that.
